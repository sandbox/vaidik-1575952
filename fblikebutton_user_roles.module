<?php

/**
 * Implements hook_user().
 */
function fblikebutton_user_roles_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'view' && user_access('access fblikebutton') && _fblikebutton_user_roles_check_role($account->roles)) {
    $webpage_to_like = url('users/' . $account->name, array('absolute' => TRUE));
    $conf = array(
      'layout' => variable_get('fblikebutton_layout', 'standard'),
      'action' => variable_get('fblikebutton_action', 'like'),
      'color_scheme' => variable_get('fblikebutton_color_scheme', 'light'),
      'show_faces' => variable_get('fblikebutton_show_faces', 'true'),
      'font' => variable_get('fblikebutton_font', 'arial'),
      'height' => variable_get('fblikebutton_iframe_height', '80'),
      'width' => variable_get('fblikebutton_iframe_width', '450'),
      'send' => variable_get('fblikebutton_send', 'true'),
      'other_css' => variable_get('fblikebutton_iframe_css', ''),
      'language' => variable_get('fblikebutton_language', 'en_US'),
    );

    $account->content['facebook'] = array(
      '#weight' => 11,
      '#attributes' => array(),
      '#type' => 'user_profile_category',
      'facebook_like' => array(
        '#type' => 'user_profile_item',
        '#value' =>  _fblikebutton_field($webpage_to_like, $conf),
        '#attributes' => array(),
      ),
    );
  }
}

/**
 * Implements hook_menu.
 */
function fblikebutton_user_roles_menu() {
  $menu = array();

  $menu['admin/settings/fblikebutton/user_roles'] = array(
    'title' => 'FB Like Button Configuration for User Roles',
    'description' => 'Configure the Facebook Like button to add it to User Profiles of specific user roles',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fblikebutton_user_roles_admin'),
    'access arguments' => array('administer fblikebutton'),
    'position' => 'right',
    'type' => MENU_NORMAL_ITEM,
    'weight' => 10,
  );

  return $menu;
}

/**
 * Menu callback for drupal_get_form.
 */
function fblikebutton_user_roles_admin() {
  $form = array();

  $form['fblikebutton_user_roles_description'] = array(
    '#value' => '<p>' . t('Select the User Roles to add the "Facebook Like" button to. All other settings that have been set for the <a href="@fblikemodule">FBLikeButton</a> module will be used.', array('@fblikemodule' => url('admin/settings/fblikebutton'))) . '</p>',
  );

  $form['fblikebutton_dynamic_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility settings'),
    '#collapsible' => FALSE,
  );

  $fblikebutton_options = user_roles(TRUE);
  $form['fblikebutton_dynamic_visibility']['fblikebutton_user_roles_selected_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Like button on these User Roles:'),
    '#options' => $fblikebutton_options,
    '#default_value' => variable_get('fblikebutton_user_roles_selected_roles', array()),
    '#description' => t('Profiles of users who belong to these user roles will have the "like" button automatically added to them.'),
  );

  return system_settings_form($form);
}

/**
 * To verify if the user profile currently being viewed is selected to add the
 * like button or not.
 */
function _fblikebutton_user_roles_check_role($user_roles) {
  $selected_roles = variable_get('fblikebutton_user_roles_selected_roles', array());
  $roles = user_roles(TRUE);

  foreach ($selected_roles as $key => $val) {
    if ($val) {
      foreach ($user_roles as $u_key => $u_val) {
        if ($roles[$val] == $u_val) {
          return true;
        }
      }
    }
  }

  return false;
}
